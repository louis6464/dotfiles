#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

export QT_QPA_PLATFORMTHEME=qt5ct
export MMPAT_PATH_TO_CFG="/etc/timidity/" # For mpv midi playback
export _JAVA_OPTIONS=-Djava.util.prefs.userRoot="$XDG_CONFIG_HOME"/java

export CUDA_VISIBLE_DEVICES=0 
# Cleanup

source ~/.config/zsh/aliases.zsh
alias x="startx"
#PS1='[\u@\h \W]\$ '

export PS1="\[\e[0;36m\]\u \[\e[0;32m\]\w \[\e[0;31m\]> \[\e[0m\]"
#eval "$(starship init bash)"
