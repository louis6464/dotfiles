## ABBR/ALIAS ###
#abbr up 'sudo pacman -Syu'
abbr inst 'sudo pacman -S'
abbr aurup 'paru -Sua'
abbr aur 'paru -S'
abbr pacrem 'sudo pacman -Rs'
abbr pacsear 'pacman -Ss'
abbr pacup 'sudo pacman -Syu'
abbr pym 'python -m'

# Typing mistakes
abbr systemclt 'systemctl'

# Alias for dotfiles bare git repo
alias config='/usr/bin/env git --git-dir=$HOME/dotfiles/ --work-tree=$HOME' 

# Blingbling
alias ls='exa -la --color=always --sort=type --icons'
alias ll='exa -l --color=always --sort=type --icons'
alias l='exa --color=always --sort=type --icons'
alias la='exa -a --color=always --sort=type --icons'

alias vim='nvim'
alias v='nvim'
alias clear='clear'
alias cls='/usr/bin/env clear'
alias say='fortune | cowsay | lolcat'

# Adding flags
alias cp="cp -i"            # Ask before overwriting
alias mv='mv -i'            # Ask before overwriting
alias rm='rm -i'            # Ask before deleting
alias df='df -h'            # Human readable 
alias free='free -m'        # Show sizes in MB
alias wcl='wc -l'
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'
alias fd='fd --hidden --color=always'

# Convenience
alias mntphone='simple-mtpfs --device 1 mnt/'
alias keyfix='setxkbmap fr -option caps:swapescape -option compose:menu'
alias audiofix='~/.config/audiofix.sh'
alias py='python'
alias cleanup='sudo pacman -Rns (pacman -Qtdq)'
alias vpnon='sudo systemctl enable expressvpn --now'
alias vpnoff='expressvpn disconnect; sudo systemctl disable expressvpn --now'
alias vpncon='expressvpn connect'
alias vpndis='expressvpn disconnect'

# alias templeos='echo "F*cking CIA n*ggers..." && aplay -q ~/.config/fish/templeos.wav' # The most important
alias templeos 'echo "F*cking CIA n*ggers..." && ffplay -nodisp ~/.config/fish/templeos.opus &>/dev/null' # Works with mp3
alias ..='cd ..'
alias ...='cd ../..'
alias .3='cd ../../..'
alias .4='cd ../../../..'
alias .5='cd ../../../../..'

# youtube-dl
alias ytdl='youtube-dl'
alias ytdla="youtube-dl --extract-audio --audio-format best"
alias ytdlao="youtube-dl --extract-audio --audio-format opus "

alias ytdlp='yt-dlp'

# gpg encryption
# verify signature for isos
alias gpg-check="gpg2 --keyserver-options auto-key-retrieve --verify"
# receive the key of a developer
